#include "./base/base.h"
#include "./os/os.h"
#include "8086_definitions.h"

#define m_alloc os_alloc
#define m_free os_free

int
main(int arg_count, char **arg_vector)
{
#if 0
	if (arg_count != 2) {
		os_printf("Missing argument\n");
		return 0;
	}
#endif
    
	M_Arena *g_arena = m_create_arena(1024*1024*16);
    
	u64 data_size = 0;
	//String8 path = str8_cstr(arg_vector[1]);
	String8 path = str8_lit("test");
	u8 *data_ptr = (u8 *)os_file_read_all(g_arena, path, &data_size);
	if (!data_ptr) {
		os_printf("Unable to read file\n");
		return 0;
	}
	u8 *pointer = data_ptr;
    
#define CHECK_POINTER() if((u64)pointer - (u64)data_ptr == data_size) goto end_loop
    
	printf("bits 16\n\n");
	while (*pointer) {
		u8 byte0 = *pointer++;
        
        Instruction instruction = {0};
        
		if ((byte0 & OPCODE_MASK) == MOV_R2R_OP) { // Register to register MOV
			CHECK_POINTER();
			u8 byte1 = *pointer++;
            
			u8 d = (byte0 >> 1) & 1;
			u8 w = byte0 & 1;
			u8 mod = byte1 >> 6;
			u8 reg = (byte1 >> 3) & 7;
			u8 r_m = byte1 & 7;
            
            instruction.opcode = byte0 & OPCODE_MASK;
            instruction.encoding = Encoding_REGISTER_MEMORY;
            instruction.flags |= (w ? IF_W : 0);
            
            Operand a = {
                .type = Operand_REG,
                .r_m = reg | (w << 3),
            };
            
            Operand b = {
                .type = Operand_MEM,
                .r_m = r_m,
            };
            
            if (mod == 0) {
                if (r_m == 0b110) {
                    CHECK_POINTER();
                    u16 disp_lo = *pointer++;
                    CHECK_POINTER();
                    u16 disp_hi = *pointer++;
                    u16 addr = disp_lo | (disp_hi << 8);
                    
                    b.offset = addr;
                    b.type = Operand_MEM_ABS;
                }
            } else if (mod == 3) {
                b.type = Operand_REG;
                b.r_m = r_m | (w << 3);
            } else {
                CHECK_POINTER();
                u16 addr = *pointer++;
                if (mod == 2) {
                    CHECK_POINTER();
                    addr |= (*pointer++) << 8;
                } else {
                    if (addr & 0x80) {
                        addr |= 0xFF00;
                    }
                }
                b.offset = addr;
            }
            
            instruction.operand_l = d ? a : b;
            instruction.operand_r = d ? b : a;
		} else if ((byte0 & OPCODE_MASK) == MOV_I2M_OP) { // Imediate to register/memory MOV
			CHECK_POINTER();
			u8 byte1 = *pointer++;
            
			u8 w = byte0 & 1;
			u8 mod = byte1 >> 6;
			u8 r_m = byte1 & 7;
            
            instruction.opcode = byte0 & OPCODE_MASK;
            instruction.encoding = Encoding_IMEDIATE_MEMORY;
            instruction.flags |= (w ? IF_W : 0);
            
            Operand a = {
                .type = Operand_MEM,
                .r_m = r_m,
            };
            
            if (mod == 0) {
                if (r_m == 0b110) {
                    CHECK_POINTER();
                    u16 disp_lo = *pointer++;
                    CHECK_POINTER();
                    u16 disp_hi = *pointer++;
                    u16 addr = disp_lo | (disp_hi << 8);
                    
                    a.offset = addr;
                    a.type = Operand_MEM_ABS;
                }
            } else if (mod == 3) {
                a.type = Operand_REG;
                a.r_m = r_m | (w << 3);
            } else {
                CHECK_POINTER();
                u16 addr = *pointer++;
                if (mod == 2) {
                    CHECK_POINTER();
                    addr |= (*pointer++) << 8;
                } else {
                    if (addr & 0x80) {
                        addr |= 0xFF00;
                    }
                }
                a.offset = addr;
            }
            
            CHECK_POINTER();
            u16 im = *pointer++;
            if (w) {
                CHECK_POINTER();
                im |= (*pointer++) << 8;
            } else if (im & 0x80) {
                im |= 0xFF00;
            }
            
            Operand b = {
                .type = Operand_IM,
                .offset = im,
            };
            
            instruction.operand_l = a;
            instruction.operand_r = b;
		} else if ((byte0 & 0xf0) == MOV_I2R_OP) { // Imediate to register MOV
            CHECK_POINTER();
			u16 data = *pointer++;
			Register reg = (Register)(byte0 & 0b1111);
            if (byte0 & 0b1000) {
				CHECK_POINTER();
				data = data | ((u16)(*pointer++) << 8);
			}
            
            instruction.opcode = byte0 & OPCODE_MASK;
            instruction.encoding = Encoding_IMEDIATE_REGISTER;
            
            instruction.operand_l = (Operand){
                .type = Operand_REG,
                .r_m = reg,
            };
            instruction.operand_r = (Operand){
                .type = Operand_IM,
                .offset = data,
            };
            
		} else if ((byte0 & OPCODE_MASK) == MOV_M2A_OP) { // Memory to acumulator MOV
			u8 invert = (byte0 & 0b10); // If not 0b10 Acumulator to Memory
            
            CHECK_POINTER();
            u16 addr = *pointer++;
            b32 w = byte0 & 1;
            
            if (w) {
                CHECK_POINTER();
                addr |= (*pointer++) << 8;
            }
            
            instruction.opcode = byte0 & OPCODE_MASK;
            instruction.encoding = Encoding_MEMORY_ACC;
            instruction.flags |= (w ? IF_W : 0);
            
            Operand a = {
                .type = Operand_REG,
                .r_m = Reg_AX,
            };
            
            Operand b = {
                .type = Operand_MEM_ABS,
                .offset = addr,
            };
            
            instruction.operand_l = invert ? b : a;
            instruction.operand_r = invert ? a : b;
		}
        
        print_instruction(instruction);
	}
    
#undef CHECK_POINTER
    end_loop:
    
	return 0;
}


#include "base/base.c"
#include "os/linux/os_linux.c"
#include "os/os.c"
