#include "../os_window.h"

#undef function
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>

#include <GL/glx.h>
#define function static

Display *display;
Window   window;
Atom     wm_delete_window;

// TODO: Add all inputs
function OS_KeyCode
x11_translate_keysym(int key)
{
	switch (key) {
		case XK_space:      return OSKC_Space;
		case XK_apostrophe: return OSKC_Apostrophe;
		case XK_comma:      return OSKC_Comma;
		case XK_minus:      return OSKC_Minus; 
		case XK_period:     return OSKC_Period;
		case XK_slash:      return OSKC_Period;
        case XK_Right:      return OSKC_Right;
        case XK_Left:       return OSKC_Left;
		case XK_0:
		case XK_1:
		case XK_2:
		case XK_3:
		case XK_4:
		case XK_5:
		case XK_6:
		case XK_7:
		case XK_8:
		case XK_9:
        return (OS_KeyCode)key;
        
		case XK_semicolon: return OSKC_Semicolon;
		case XK_equal:     return OSKC_Equal;
        
		case XK_A:
		case XK_a:
        return OSKC_A;
		case XK_B:
		case XK_b:
        return OSKC_B;
		case XK_C:
		case XK_c:
        return OSKC_C;
		case XK_D:
		case XK_d:
        return OSKC_D;
		case XK_E:
		case XK_e:
        return OSKC_E;
		case XK_F:
		case XK_f:
        return OSKC_F;
		case XK_G:
		case XK_g:
        return OSKC_G;
		case XK_H:
		case XK_h:
        return OSKC_H;
		case XK_I:
		case XK_i:
        return OSKC_I;
		case XK_J:
		case XK_j:
        return OSKC_J;
		case XK_K:
		case XK_k:
        return OSKC_K;
		case XK_L:
		case XK_l:
        return OSKC_L;
		case XK_M:
		case XK_m:
        return OSKC_M;
		case XK_N:
		case XK_n:
        return OSKC_N;
		case XK_O:
		case XK_o:
        return OSKC_O;
		case XK_P:
		case XK_p:
        return OSKC_P;
		case XK_Q:
		case XK_q:
        return OSKC_Q;
		case XK_R:
		case XK_r:
        return OSKC_R;
		case XK_S:
		case XK_s:
        return OSKC_S;
		case XK_T:
		case XK_t:
        return OSKC_T;
		case XK_U:
		case XK_u:
        return OSKC_U;
		case XK_V:
		case XK_v:
        return OSKC_V;
		case XK_W:
		case XK_w:
        return OSKC_W;
		case XK_X:
		case XK_x:
        return OSKC_X;
		case XK_Y:
		case XK_y:
        return OSKC_Y;
		case XK_Z:
		case XK_z:
        return OSKC_Z;
		case XK_bracketleft:  return OSKC_LeftBracket;
		case XK_bracketright: return OSKC_RightBracket;
		case XK_grave:        return OSKC_GraveAccent;
        
        case XK_F1:  return OSKC_F1;
        case XK_F2:  return OSKC_F2;
        case XK_F3:  return OSKC_F3;
        case XK_F4:  return OSKC_F4;
        case XK_F5:  return OSKC_F5;
        case XK_F6:  return OSKC_F6;
        case XK_F7:  return OSKC_F7;
        case XK_F8:  return OSKC_F8;
        case XK_F9:  return OSKC_F9;
        case XK_F10: return OSKC_F10;
        case XK_F11: return OSKC_F11;
        case XK_F12: return OSKC_F12;
        
		default:
        return OSKC_Unknown;
	}
    
}

global u8 _os_text_input_buffer[256];
global u64 _os_text_input_buffer_len = 0;

function OS_Event
os_next_event()
{
	OS_Event event = {};
	if (XPending(display) == 0) {
		return event;
	}
    
	XEvent xevent = {};
	XNextEvent(display, &xevent);
    
	switch (xevent.type) {
		case Expose: {
			event.type = OSE_WindowResize;
			XWindowAttributes attrib = {};
			XGetWindowAttributes(display, window, &attrib);
			event.width = attrib.width;
			event.height = attrib.height;
		} break;
        
		case KeyPress: {
			event.type = OSE_KeyPress;
            
            
			auto keysym = XkbKeycodeToKeysym(display, xevent.xkey.keycode, 0,
                                             xevent.xkey.state & (ShiftMask ? 1 : 0));
            
            XComposeStatus status;
            _os_text_input_buffer_len = XLookupString((XKeyPressedEvent *)&xevent, (char*)_os_text_input_buffer, 255, 0, &status);
            _os_text_input_buffer[_os_text_input_buffer_len] = 0;
            
			event.keycode = x11_translate_keysym(keysym);
		} break;
        
		case KeyRelease: {
			event.type = OSE_KeyRelease;
			auto keysym = XkbKeycodeToKeysym(display, xevent.xkey.keycode, 0,
											 xevent.xkey.state & (ShiftMask ? 1 : 0));
			event.keycode = x11_translate_keysym(keysym);
		} break;
        
		case ButtonPress: {
            
            if (xevent.xbutton.button < 4 || xevent.xbutton.button > 5) {
                event.type = OSE_ButtonPress;
                event.button = xevent.xbutton.button;
            } else {
                event.type = OSE_MouseWheel;
                event.motion_y = xevent.xbutton.button == 4 ? 1.0f : -1.0f;
            }
		} break;
        
		case ButtonRelease: {
			event.type = OSE_ButtonRelease;
			event.button = xevent.xbutton.button;
		} break;
        
		case MotionNotify: {
			event.type = OSE_CursorMotion;
			event.motion_x = xevent.xmotion.x;
			event.motion_y = xevent.xmotion.y;
		} break;	
        
		case ClientMessage: {
			if (xevent.xclient.data.l[0] == wm_delete_window) {
                event.type = OSE_WindowClose;
				XDestroyWindow(display, window);
			}
		} break;
	}
    
	return event;
}

function WindowHandle
os_create_window_with_gctx(const char *name, int width, int height)
{
	display = XOpenDisplay(0);
    
	if (!display) {
		os_printf("ERROR: Unable to open X Display\n");
		return 0;
	}
    
	auto root = DefaultRootWindow(display);
    
	GLint att[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
	auto visual = glXChooseVisual(display, DefaultScreen(display), att);
    
	if (!visual) {
		os_printf("ERROR: Unable to find visual\n");
		return 0;
	}
    
	auto colormap = XCreateColormap(display, root, visual->visual, AllocNone);
    
	XSetWindowAttributes swa = {};
	swa.colormap = colormap;
	swa.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask;
    
	window = XCreateWindow(display, root, 0, 0, e_ctx.window_size.x, e_ctx.window_size.y, 0, visual->depth, InputOutput, visual->visual, CWColormap | CWEventMask, &swa);
    
    if (!window) {
        os_printf("ERROR: Unabel to create window\n");
        return 0;
    }
    
	XMapWindow(display, window);
	XStoreName(display, window, name);
    
	wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", 0);
	if (!XSetWMProtocols(display, window, &wm_delete_window, 1)) {
		os_printf("ERROR: Unable to set window shutdown atom\n");
		return 0;
	}
    
	auto gl_context = glXCreateContext(display, visual, 0, GL_TRUE);
	glXMakeCurrent(display, window, gl_context);
    
	return 1;
}

function void
os_window_swap()
{
	glXSwapBuffers(display, window);
}


function String8
os_text_input()
{
    return str8(_os_text_input_buffer, _os_text_input_buffer_len);
}

function void
os_clear_text_buffer()
{
    _os_text_input_buffer_len = 0;
}