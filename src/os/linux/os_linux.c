#include <sys/mman.h>
#include <stdio.h>

function void *
os_reserve(u64 size)
{
	void *mem = mmap(0, size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
	return mem;
}

function void
os_commit(void *ptr, u64 size)
{

}

function void
os_decommit(void *ptr, u64 size)
{

}

function void
os_release(void *ptr, u64 size)
{
	munmap(ptr, size);
}

function void *
os_alloc(u64 size)
{
	return os_reserve(size);
}

function void
os_free(void *mem, u64 size)
{
	os_release(mem, size);
}

function double
os_current_time()
{
	// TODO: Implement this function
	return 0;
}