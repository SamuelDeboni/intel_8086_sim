#include <stdio.h>
#include <time.h>

// TODO: Replace CRTL
function void *
os_file_read_all_mem(void *mem, String8 path, u64 *size)
{
    if (path.len > 1024) return 0;
    
    
    u8 buff[1024];
    M_Arena *tmp_arena = m_create_arena_mem(buff, 1024);
    path = str8_copy(tmp_arena, path);
    
    
    FILE *fn = fopen((const char *)path.str, "rb");
    
    if (!fn) {
        return 0;
    }
    
    fseek(fn, 0, SEEK_END);
    *size = ftell(fn);
    fseek(fn, 0, SEEK_SET);
    
    if (mem) {
        fread(mem, 1, *size, fn);   
    }
    
    fclose(fn);
    
    return mem;
}

function void *
os_file_read_all(M_Arena *arena, String8 path, u64 *size)
{
    u64 file_size = 0;
    
    os_file_read_all_mem((void *)0, path, &file_size);
    void *mem = os_file_read_all_mem(m_arena_push(arena, file_size+1), path, &file_size);
    *size = file_size;
    return mem;
}


function void
os_file_write_all(String8 path, String8 data)
{
    u8 buff[1024];
    M_Arena *tmp_arena = m_create_arena_mem(buff, 1024);
    path = str8_copy(tmp_arena, path);
    
    os_printf("save file at: %s\n", path.str);
    
    FILE *fn = fopen((const char *)path.str, "wb");
    fwrite(data.str, 1, data.len, fn);
    fclose(fn);
}

// TODO(samuel): Add flags support
function OS_File
os_file_open(String8 path, u32 flags)
{
    u8 buff[1024];
    M_Arena *tmp_arena = m_create_arena_mem(buff, 1024);
    path = str8_copy(tmp_arena, path);
    
    FILE *fn = fopen((const char *)path.str, "wb");
    return (OS_File)fn;
}

function void
os_file_write(OS_File file, String8 data)
{
    fwrite(data.str, 1, data.len, (FILE *)file);
}

function void
os_file_close(OS_File file)
{
    fclose((FILE *)file);
}
