#include <stdlib.h>

function void *
os_reserve(u64 size)
{
    void *mem = malloc(size);
    return mem;
}

function void
os_commit(void *ptr, u64 size)
{

}

function void
os_decommit(void *ptr, u64 size)
{

}

function void
os_release(void *ptr, u64 size)
{
    free(ptr);
}

function void *
os_alloc(u64 size)
{
    return os_reserve(size);
}

function void
os_free(void *mem, u64 size)
{
    os_release(mem, size);
}

#include <time.h>

function double
os_current_time()
{
    double seconds = (double)(clock()) / CLOCKS_PER_SEC;
    return seconds;
}