#undef function
#include <windows.h>
#define function static

global OS_Event _os_event = {};

global u8 _os_text_input_buffer[256];
global u64 _os_text_input_buffer_len = 0;


function OS_KeyCode
win32_translate_key(WPARAM w_param)
{
    switch (w_param) {
        case VK_SHIFT:   return OSKC_LeftShift;
        case VK_CONTROL: return OSKC_LeftControl;
        case VK_ESCAPE:  return OSKC_Escape;
        case VK_SPACE:   return OSKC_Space;
        case VK_UP:      return OSKC_Up;
        case VK_RIGHT:   return OSKC_Right;
        case VK_DOWN:    return OSKC_Down;
        case VK_LEFT:    return OSKC_Left;
        
        default: {
            if (w_param >= 'A' && w_param <= 'Z' || w_param >= '0' && w_param <= '9') {
                return (OS_KeyCode)w_param;
            }
            
            return OSKC_Unknown;
        } break;
        
        case VK_F1:  return OSKC_F1;
        case VK_F2:  return OSKC_F2;
        case VK_F3:  return OSKC_F3;
        case VK_F4:  return OSKC_F4;
        case VK_F5:  return OSKC_F5;
        case VK_F6:  return OSKC_F6;
        case VK_F7:  return OSKC_F7;
        case VK_F8:  return OSKC_F8;
        case VK_F9:  return OSKC_F9;
        case VK_F10: return OSKC_F10;
        case VK_F11: return OSKC_F11;
        case VK_F12: return OSKC_F12;
    }
}

LRESULT
main_window_callback(HWND window, UINT message, WPARAM w_param, LPARAM l_param)
{
    LRESULT result = 0;
    
    switch (message) {
        case WM_KEYDOWN: {
            _os_event.type = OSE_KeyPress;
            _os_event.keycode = win32_translate_key(w_param);
        } break;
        
        case WM_KEYUP: {
            _os_event.type = OSE_KeyRelease;
            _os_event.keycode = win32_translate_key(w_param);
        } break;
        
        case WM_SIZE: {
            _os_event.type = OSE_WindowResize;
            _os_event.width = LOWORD(l_param);
            _os_event.height = HIWORD(l_param);
        } break;
        
        case WM_MOUSEMOVE: {
            _os_event.type = OSE_CursorMotion;
            _os_event.motion_x = (float)LOWORD(l_param);
            _os_event.motion_y = (float)HIWORD(l_param);
        } break;
        
        case WM_DESTROY:
        case WM_CLOSE: {
            _os_event.type = OSE_WindowClose;
        } break;
        
        case WM_MOUSEWHEEL: {
            _os_event.type = OSE_MouseWheel;
            _os_event.motion_y = (f32)GET_WHEEL_DELTA_WPARAM(w_param) / 120.0f;
        } break;
        
        // Mouse Input
        case WM_LBUTTONDOWN: {
            _os_event.type = OSE_ButtonPress;
            _os_event.button = 1;
        } break;
        case WM_MBUTTONDOWN: {
            _os_event.type = OSE_ButtonPress;
            _os_event.button = 2;
        } break;
        case WM_RBUTTONDOWN: {
            _os_event.type = OSE_ButtonPress;
            _os_event.button = 3;
        } break;
        case WM_LBUTTONUP: {
            _os_event.type = OSE_ButtonRelease;
            _os_event.button = 1;
        } break;
        case WM_MBUTTONUP: {
            _os_event.type = OSE_ButtonRelease;
            _os_event.button = 2;
        } break;
        case WM_RBUTTONUP: {
            _os_event.type = OSE_ButtonRelease;
            _os_event.button = 3;
        } break;
        
        case WM_CHAR: {
            if (_os_text_input_buffer_len < 255 && w_param < 128) {
                _os_text_input_buffer[_os_text_input_buffer_len++] = (u8)w_param;
            }
        } break;
        
        default: {
            result = DefWindowProc(window, message, w_param, l_param);
        } break;
    }
    
    return result;
}

function OS_Event
os_next_event()
{
    _os_event = {};
    
    MSG msg;
    if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
        if (msg.message == WM_QUIT) {
            _os_event.type = OSE_WindowClose;
        }
        
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    
    CURSORINFO cursor_info;
    GetCursorInfo(&cursor_info);
    
    return _os_event;
}

HDC device_context = 0;

function WindowHandle
os_create_window_with_gctx(const char *name, int width, int height)
{
    auto instance = GetModuleHandle(0);
    
    WNDCLASS window_class = {};
    window_class.style         = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    window_class.lpfnWndProc   = main_window_callback;
    window_class.hInstance     = instance;
    window_class.lpszClassName = name;
    window_class.hCursor = LoadCursor(0, IDC_ARROW);
    
    if (!RegisterClass(&window_class)) {
        // TODO: Log error
        return 0;
    }
    
    RECT rect = {};
    AdjustWindowRect(&rect, WS_TILEDWINDOW, FALSE);
    height += rect.bottom - rect.top;
    width  += rect.right  - rect.left;
    
    HWND window_handle = CreateWindowEx(0, window_class.lpszClassName, name,
                                        WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, instance, 0);
    
    if (!window_handle) {
        // TODO: Log error
        return 0;
    }
    
    PIXELFORMATDESCRIPTOR pfd = {};
    pfd.nSize        = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion     = 1;
    pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType   = PFD_TYPE_RGBA;
    pfd.cColorBits   = 32;
    pfd.cDepthBits   = 24;
    pfd.cStencilBits = 8;
    pfd.iLayerType   = PFD_MAIN_PLANE;
    
    device_context = GetDC(window_handle);
    int pixel_format = ChoosePixelFormat(device_context, &pfd);
    SetPixelFormat(device_context, pixel_format, &pfd);
    
    auto gl_context = wglCreateContext(device_context);
    wglMakeCurrent(device_context, gl_context);
    
    return 1;
}

function void
os_window_swap()
{
    SwapBuffers(device_context);
}

function String8
os_text_input()
{
    return str8(_os_text_input_buffer, _os_text_input_buffer_len);
}

function void
os_clear_text_buffer()
{
    _os_text_input_buffer_len = 0;
}