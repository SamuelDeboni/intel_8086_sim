#ifndef OS_H
#define OS_H

#include "../base/base.h"

typedef void * OS_File;

#define OS_File_NO_Read  1;
#define OS_File_NO_Write 2;

// TODO(samuel): Create my own print
#include <stdio.h>
#define os_printf printf
#define os_exit exit

function void *os_alloc(size_t size);
function void  os_free(void *mem, size_t size);

function void *os_reserve(u64 size);
function void  os_commit(void *ptr, u64 size);
function void  os_decommit(void *ptr, u64 size);
function void  os_release(void *ptr, u64 size);

function void *os_file_read_all_mem(void *mem, String8 path, u64 *size);
function void *os_file_read_all(M_Arena *arena, String8 path, u64 *size);
function void  os_file_write_all(String8 path, String8 data);

function OS_File os_file_open(String8 path, u32 flags);
function void os_file_write(OS_File file, String8 data);
function void os_file_close(OS_File file);

function double os_current_time();

#define m_reserve  os_reserve
#define m_commit   os_commit
#define m_decommit os_decommit
#define m_release  os_release

#endif // OS_H