/* date = August 26th 2022 5:14 pm */

#ifndef BASE_MATH_H
#define BASE_MATH_H

#include "base_types.h"

function double cos(double x);
function double sin(double x);

#endif //BASE_MATH_H
