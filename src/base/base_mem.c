#include "base_mem.h"

#if (!defined(m_alloc) || !defined(m_free))
#include <stdlib.h>
function void* m_alloc(u64 size) {
	return malloc(size);
}
function void m_free(void *ptr, u64 size) {
	free(ptr);
}
#endif

function M_Arena *
m_create_arena_mem(void *mem, u64 capacity)
{
	M_Arena *arena = (M_Arena *)mem;
	arena->capacity    = capacity;
	arena->offset      = sizeof(M_Arena);
	arena->prev_offset = arena->offset;
	return arena;
}

function M_Arena *
m_create_arena(u64 capacity)
{
	void *mem = m_alloc(capacity);
	return m_create_arena_mem(mem, capacity);
}

function void
m_arena_release(M_Arena *arena)
{
	ASSERT(arena);
	m_free((void *)arena, arena->capacity);
}

function void
m_arena_reset(M_Arena *arena)
{
	ASSERT(arena);
	arena->offset      = sizeof(M_Arena);
	arena->prev_offset = arena->offset;
}

function u64
m_align_forward(u64 ptr, u64 align)
{
	u64 p = ptr;
	u64 a = (u64)align;
	u64 modulo = p & (a - 1);

	if (modulo != 0) {
		p += a - modulo;
	}

	return p;
}

function void *
m_arena_push(M_Arena *arena, u64 size)
{
	void *result = 0;

	u64 offset = (u64)arena + (u64)arena->offset;
	offset = m_align_forward(offset, DEFAULT_ALIGN);
	offset -= (u64)arena;

	if (offset + size <= arena->capacity) {
		result = (u8 *)arena + offset;
		arena->prev_offset = arena->offset;
		arena->offset = offset + size;
		MEMSET((u8 *)result, 0, size);

		for (u64 i = 0; i < size; i++) ((u8 *)result)[i] = 0;
	}

	return result;
}

function void
m_arena_pop(M_Arena *arena)
{
	arena->offset = arena->prev_offset;
}

function void
m_arena_pop_to(M_Arena *arena, u64 offset)
{
	arena->offset = m_align_forward(offset + (u64)arena, DEFAULT_ALIGN) - (u64)arena;
}

function void
m_arena_pop_count(M_Arena *arena, u64 count)
{
	m_arena_pop_to(arena, arena->offset - count);
}
