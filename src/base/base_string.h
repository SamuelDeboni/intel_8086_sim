/* date = October 29th 2020 3:19 pm */
#include "base_types.h"
#include "base_mem.h"

#ifndef BASE_STRING_H
#define BASE_STRING_H

#define SFOR(s) for (uint32_t i = 0, it = s.str[0]; \
i < s.len; \
it = s.str[++i])

// ===== Definitions =====

// NOTE(samuel): Always have len+1 and null terminate for cstring compatibility
typedef struct String8 String8;
struct String8
{
	u8 *str;
	u64 len;
};

typedef struct String8Node String8Node;
struct String8Node
{
	String8 str;
	String8Node *next;
	String8Node *prev;
};

typedef struct String8List String8List;
struct String8List
{
	u64 len;
	String8Node *first;
	String8Node *last;
};

function inline void
str8_offset(String8 *s, i64 offset)
{
	s->str += offset;
	s->len -= offset;
}

//~ Constructors
#define str8_lit(s) str8((u8 *)(s), sizeof((s)) - 1)

//~ String Functions
function String8     str8(u8 *buffer, u64 len);
function String8     str8_cstr(char *cstr);

function String8     str8_trim(String8 str);
function b32         str8_equals(String8 s1, String8 s2);
function String8     str8_copy(M_Arena *arena, String8 str);
function String8     str8_substr(String8 str, u64 start_idx, u64 end_idx);
function String8List str8_split(M_Arena *arena, String8 str, u8 separator);


//~ String list functions
function void    str8_push_front(M_Arena *arena, String8List *list, String8 str);
function String8 str8_pop_front(M_Arena *arena, String8List *list);

function void    str8_push_back(M_Arena *arena, String8List *list, String8 str);
function String8 str8_pop_back(M_Arena *arena, String8List *list);

//~ Conversion
function b32 str8_to_i64_decimal(String8 str, i64 *out);
function b32 str8_to_f64(String8 str, f64 *out);


#endif //DEBONI_STRING_H