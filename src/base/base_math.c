#include "base.h"

double cos(double x) {
	if (x < 0) {
		int q = -x / TAU;
		q += 1;
		double y = q * TAU;
		x = -(x - y);
	}

	if (x >= TAU) {
		int q = x / TAU;
		double y = q * TAU;
		x = x - y;
	}

	int s = 1;
	if (x >= PI) {
		s = -1;
		x -= PI;
	}

	if (x > HALF_PI) {
		x = PI - x;
		s = -s;
	}

	double z = x * x;
	double r = z * (z * (SIN_CURVE_A - SIN_CURVE_B * z) - 0.5) + 1.0;

	r -= (double)((int)(r > 1.0) * 2);

	return r * s;
}

double sin(double x) {
	return cos(x - HALF_PI);
}
