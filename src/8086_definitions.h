/* date = March 10th 2023 9:34 am */

#ifndef _8086_DEFINITIONS_H
#define _8086_DEFINITIONS_H


typedef enum Register {
	Reg_AL = 0,
	Reg_CL,
	Reg_DL,
	Reg_BL,
	Reg_AH,
	Reg_CH,
	Reg_DH,
	Reg_BH,
	Reg_AX,
	Reg_CX,
	Reg_DX,
	Reg_BX,
	Reg_SP,
	Reg_BP,
	Reg_SI,
	Reg_DI,
} Register;

char *(register_name_map[16]) = {
	"al",
	"cl",
	"dl",
	"bl",
	"ah",
	"ch",
	"dh",
	"bh",
    
	"ax",
	"cx",
	"dx",
	"bx",
	"sp",
	"bp",
	"si",
	"di",
};

char *(eaddr_name_table[8]) = {
	"bx + si",
	"bx + di",
	"bp + si",
	"bp + di",
	"si",
	"di",
	"bp",
	"bx",
};


#define OPCODE_MASK 0b11111100

#define MOV_R2R_OP  0b10001000
#define MOV_I2M_OP  0b11000100
#define MOV_I2R_OP  0b10110000
#define MOV_M2A_OP  0b10100000

typedef enum InstructionEncoding {
    Encoding_REGISTER_MEMORY,
    Encoding_IMEDIATE_MEMORY,
    Encoding_IMEDIATE_REGISTER,
    Encoding_MEMORY_ACC,
} InstructionEncoding;

typedef enum OperandType {
    Operand_NULL,
    Operand_IM,
    Operand_REG,
    Operand_MEM,
    Operand_MEM_ABS,
} OperandType;

typedef struct Operand {
    OperandType type;
    u8 r_m;
    u16 offset;
} Operand;

#define IF_W 0x1

typedef struct Instruction {
    InstructionEncoding encoding;
    u8 opcode;
    Operand operand_l, operand_r;
    b8 flags;
} Instruction;


function void
print_instruction(Instruction instruction) {
    printf("%s ", "mov");
    
    Operand operand = instruction.operand_l;
    switch (operand.type) {
        case Operand_REG: {
            printf("%s", register_name_map[operand.r_m]);
        } break;
        
        case Operand_MEM: {
            if (!operand.offset) {
                printf("[%s]", eaddr_name_table[operand.r_m]);
            } else {
                if ((i16)operand.offset < 0) {
                    printf("[%s - %d]", eaddr_name_table[operand.r_m], -(i16)operand.offset);
                } else {
                    printf("[%s + %d]", eaddr_name_table[operand.r_m], (i16)operand.offset);
                }
            }
        } break;
        
        case Operand_MEM_ABS: {
            printf("[%d]", operand.offset);
        } break;
        
    }
    
    printf(", ");
    
    operand = instruction.operand_r;
    switch (operand.type) {
        case Operand_IM: {
            if (instruction.encoding == Encoding_IMEDIATE_MEMORY) {
                if (instruction.flags & IF_W) {
                    printf("word %d", operand.offset);
                } else {
                    printf("byte %d", operand.offset);
                }
            } else {
                printf("%d", operand.offset);
            }
        } break;
        
        case Operand_REG: {
            printf("%s", register_name_map[operand.r_m]);
        } break;
        
        case Operand_MEM: {
            if (!operand.offset) {
                printf("[%s]", eaddr_name_table[operand.r_m]);
            } else {
                if ((i16)operand.offset < 0) {
                    printf("[%s - %d]", eaddr_name_table[operand.r_m], -(i16)operand.offset);
                } else {
                    printf("[%s + %d]", eaddr_name_table[operand.r_m], (i16)operand.offset);
                }
            }
        } break;
        
        case Operand_MEM_ABS: {
            printf("[%d]", operand.offset);
        } break;
        
    }
    
    printf("\n");
}

#endif //8086_DEFINITIONS_H
