#!/bin/sh

test_file=$1

./build.sh

cp $test_file test.asm

echo "assembling test file"
nasm test.asm

echo "genereting output"
./8086_sim > out.asm

echo "assembling out file"
nasm out.asm

echo "comparing out with test"
diff out test