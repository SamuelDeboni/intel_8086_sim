#!/bin/bash

code="$PWD"
opts='-g -std=c11'
cd . > /dev/null
gcc $opts $code/src/main.c -o 8086_sim
cd $code > /dev/null
